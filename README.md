#### ℹ️ About

This is a bash wrapper for OpenMW's portable mode as well as a few essential modding tools. Using this allows you to maintain multiple, parallel modlist configurations along with their own patchsets. You can also edit configs and settings, create patches, validate configs, spawn into test saves, quickly browse screenshots, and much more. Just run `omw --help` to see the possibilities and navigate all things OpenMW on your system!

Being a bash script, `omw` is made for GNU/Linux, but WSL is supported as well.

#### ⚠️ Disclaimer

This is not a mod manager. For that, you will need [`umo`](https://modding-openmw.gitlab.io/umo/). This will not create configs for you. For that, you will need [`momw-configurator`](https://modding-openmw.gitlab.io/momw-configurator/). All of these tools, including ones required for this program, are contained within the [MOMW Tools Pack](https://modding-openmw.gitlab.io/momw-tools-pack/).

#### ☑️ Prerequisites


- 🌒 [**Morrowind**](https://elderscrolls.bethesda.net/en/morrowind) [[Steam](https://store.steampowered.com/app/22320/The_Elder_Scrolls_III_Morrowind_Game_of_the_Year_Edition)|[GoG](https://www.gog.com/en/game/the_elder_scrolls_iii_morrowind_goty_edition)]
- 🕹️ [**OpenMW**](https://openmw.org/downloads) `0.48`+ 
  - The [AppImage](https://modding-openmw.com/mods/openmw-appimage) is great for dev versions and/or immutable distros.
  - The Flatpak is **not** recommended under any circumstance!
- 🪛 [**MOMW Tools Pack**](https://modding-openmw.gitlab.io/momw-tools-pack/)
  - [DeltaPlugin](https://gitlab.com/bmwinger/delta-plugin/-/releases)
  - [Groundcoverify!](https://gitlab.com/bmwinger/groundcoverify/-/releases)
  - [S3LightFixes](https://github.com/magicaldave/S3LightFixes/releases)
  - [openmw-validator](https://modding-openmw.gitlab.io/openmw-validator)
- 📁 **File Browser** — I recommend [`thunar`](https://docs.xfce.org/xfce/thunar/start).
- 🌐 **Web Browser** — I recommend [`firefox`](https://www.mozilla.org/en-US/firefox/enterprise/#download).
- 🗒️ **Text Editor** — I recommend [`vscodium`](https://github.com/VSCodium/vscodium/releases).

#### 📦️ Initial Setup

Download the package and extract `omw` to somewhere in your `$PATH`, e.g.:

    /usr/local/bin/omw

Extract the included `Profiles` folder wherever you'd like to keep your OpenMW profile data:

    ~/games/OpenMW/Profiles

Open `omw` in a text editor and modify the `directory` variable to point where you placed the `Profiles` directory:

    # omw
    ...
    ######################
     # USER CONFIGURATION #
      ######################

    directory="$HOME/games/OpenMW/Profiles"
    ...
- ➕ **New profiles** will be duplicated from whatever is in the `Default` profile directory.
- ✔️ **At a minimum**, profiles must contain an `openmw.cfg` file and a `settings.cfg` file.
- ⚙️ **Set up** the rest of the proceeding variables to match your system's paths, based on your specific needs (see: [**Configuration**](#configuration)).
- 🪟 **Windows users with WSL** should use the linux directory structure **exclusively**.
  - E.g. `C:\Users\gregoire` becomes `/mnt/c/Users/gregoire`.
  - Use `wslpath` command if you're not sure, and don't forget the file extensions when necessary (notably `.exe` for all executables, including `python`).

Create a new profile:

    $ omw -p TotalOverhaul create

- 🔁 You can specify an alternative profile path on the fly with `-d`.
- 🧩 You can specify an alternative OpenMW binary on the fly with `-b`.
- 🗑️ To rename or delete a profile, simply rename or delete its directory.

Check your `openmw.cfg`:

    $ omw -p TotalOverhaul config

- ➡️ The contents of the `Default` folder will be copied to your new profile folder.
- 🔧 You can modify the contents of the `Default` folder to make creating new profiles faster and easier.

#### 🏭️ How It Works

Install a [mod list](https://modding-openmw.com/lists/total-overhaul) and use the [**CFG Generator**](https://modding-openmw.com/cfg/total-overhaul).

Check for configuration errors:

    $ omw -p TotalOverhaul validate

Run your patches per profile:

    $ omw -p TotalOverhaul merge
    $ omw -p TotalOverhaul groundcover
    $ omw -p TotalOverahul lightfixes
    $ omw -p TotalOverhaul navmesh

You can even to do them all at once!

    $ omw -p TotalOverhaul generate-all

Check your settings:

    $ omw -p TotalOverhaul settings

Skip the menu, pick a cell, and test the game:

    $ omw -p TotalOverhaul -c "Old Ebonheart" test

Just play the damn game:

    $ omw -p TotalOverhaul

Launch from Steam using this in your "Launch Options":

    omw -p TotalOverhaul %command%

#### 🛠️ Configuration

- `directory` — Path of `omw` profile data.
- `template` — Name of default template folder.
- `omw` — Path of `omw` script (used for portability).
- `fm` — Your file manager program name.
- `editor` — Your editor program name.
- `binary` — Path of your `openmw` executable.
- `cs` — Path of your `openmw-cs` executable.
- `navmeshtool` — Path of your `openmw-navmeshtool` executable.
- `navmeshthreads` — Number of threads to use for navmesh generation.
- `python` — Path of `python` binary.
- `esp` — ESP or ESM of file to edit in `openmw-cs` (usually empty).
- `mods` — Path of your TESIII mods directory.
- `tools` — Path of your TESIII tools directory.
- `morrowind` — Path of your TESIII installation directory.
- `tes3cmd` — Path of your `tes3cmd` executable.
- `deltaplugin` — Path of your `delta_plugin` executable.
- `groundcoverify` — Path of your `groundcoverify.py` executable.
- `s3lightfixes` — Path of your `s3lightfixes` executable.
- `openmwvalidator` — Path of your `openmw-validator-linux-amd64` executable.
- `testcell` — Cell on which test characters will spawn.
- `testdir` — Path of your test script directory.
- `testscript` — Script that will execute before a test.
- `robowind` — Path of your Robowind AppImage.

#### 🛟 Troubleshooting

- Make sure you've set all relevant variables in the **USER CONFIGURATION** section of `omw`.
- Make sure your paths are correct again.

#### 📣 Reporting a Problem

- **GitLab** — [Create an Issue](https://gitlab.com/modding-openmw/omw/-/issues)
- **Email** — `settyness@gmail.com`
- **Mastodon** — `@Settyness@infosec.exchange`
- **Discord** — `@Settyness`

#### 🤝 Credits

- **Settyness** — author
- **johnnyhostile** — bash mentor, openmw-validator
- **bonswouar** — sed magic, WSL support
- **Benjamin Winger** — DeltaPlugin, Groundcoverify!
- **S3ctor** — S3LightFixes
- **OpenMW Team** — OpenMW
- **Bethesda** — Morrowind
