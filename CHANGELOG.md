# 📜 Changelog

Noteworthy changes are documented here.

### [1.1.10] - 2025-02-17

- Discontinue `omwllf` and `plox`
- Full path to `esp` is now needed for `cs`
- Prefer user `env`

### [1.1.10] - 2024-12-14

- Add credit for `omwllf` (sorry!)
- Change `waza_lightfixes` to `s3lightfixes`
- Fix `generate-all` duplicating subroutines' `sed` commands
- Revamp `README.md`

#### [1.1.9] - 2024-11-25

- Add `shaders` command
- Change `morrowind` variable to `game`
- Remove a few redundant vars
- Fix duplicate `project` verb
- Fix typo in changelog
- Fix command and arg formatting in changelog
- Patches now disable and re-enable themselves (and possibly others) in `openmw.cfg`
- Profiles now name themselves and insert their own `data=` paths
- Lightfixes is now portable, no more collisions possible
- Make WSL code pass shellcheck

#### [1.1.8] - 2024-11-08

- Update `wolverine` patch routine

#### [1.1.7] - 2024-11-03

- Add WSL support (thanks, bonswouar!)
- Make `omw` itself protable (thanks, bonswouar!)
- Make `python` binary path configurable (thanks, bonswouar!)
- Vastly improve `generate-all` command (thanks, bonswouar!)

#### [1.1.6] - 2024-04-28

- Add `wolverine` command
- Add `rwc` command
- Add many alternative command verbs
- Add configuration to readme
- Add troubleshooting to readme
- Add contact details to readme

#### [1.1.5] - 2024-03-17

- Update `plox` command structure
- Format/content for MOMW GitLab release

#### [1.1.4] - 2024-03-14

- Add `plox` command
- Add `cs` command to `help`
- Add `testdir` to make `script` option shorter

#### [1.1.3] - 2024-03-09

- Add `esp` option
- Add `threads` option
- Change `testplay` command to `test` command
- Fix global profile launching by default instead of default tempalte profile (this was the correct implementation)
- Fix `waza_lightfixes` overwriting global config if it exists

#### [1.1.2] - 2024-03-08

- Add `cs` command
- Change user config
- Fix code typos
- Fix default template profile launching by default instead of global profile

#### [1.1.1] - 2024-03-08

- Change formatting for better readability
- Change `profile` command to `show` command
- Change `profiles` variable to `directory`

#### [1.1.0] - 2024-03-07

- Add `binary` option
- Add `directory` option
- Add `cell` option
- Add `script` option
- Add `testplay` command
- Change `create` syntax
- Change `profiles` command to `list` command

### [1.0.0] - 2024-03-06

_The public release that uses portable instances._

#### Added Features

- Add `help` option
- Add `profile` option
- Add `profile` command
- Add `profiles` command
- Add `create` command
- Add `generate-all` command for portable scope
- Change `config` command to portable scope
- Change `settings` command to portable scope
- Change `log` command to portable scope
- Change `data` command to portable scope
- Change `plugins` command to portable scope
- Change `projects` command to portable scope
- Change `saves` command to portable scope
- Change `screenshots` command to portable scope
- Change `level` command to portable scope
- Change `merge` command to portable scope
- Change `groundcover` command to portable scope
- Change `lightfixes` command to portable scope
- Change `validate` command to portable scope

#### [beta] - 2018

_A bare bones global version for personal use only._

#### Added Features

- Add `config` command for global scope
- Add `settings` command for global scope
- Add `log` command for global scope
- Add `data` command for global scope
- Add `plugins` command for global scope
- Add `projects` command for global scope
- Add `saves` command for global scope
- Add `screenshots` command for global scope
- Add `level` command for global scope
- Add `merge` command for global scope
- Add `groundcover` command for global scope
- Add `lightfixes` command for global scope
- Add `validate` comand for global scope
- Add `mods` command
- Add `tools` command
- Add `testscript` command
